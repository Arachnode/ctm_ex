//Vars
max_msg_size = 255;
send_to_number = '+15555555555';
message = '';
send_to_number_valid = false;
message_valid = false;

//Send off an sms using svcs.php
function send_sms(){
	var base_url = 'php/svcs.php?';
	var params = {	action: 'send',
					to: 	send_to_number,
					msg: 	$('#sms_msg').val()
				};
	//Build our REST request string
	var rq_str = base_url+$.param(params);

	$.getJSON(rq_str, function(data) {
		console.log("result: "+data.status);
		if(data.status=="Success"){
			alert("Message Sent");
			$(".outer_container").find("input, textarea").val("");
		}else{
			alert("Error, please try again");
		}
    });
    console.log("SMS sent to: "+params.to);
}

//Make sure we have a valid number and message before displaying the Send button
function check_form(){
	if(message_valid && send_to_number_valid){
		$("#send").show();
	}else{
		$("#send").hide();
	}
}

//Only show the Send button if we have a valid number to send to
function validate_phone(){
	//regex will replace all non-numeric characters with blank
	var to_num = $('#target_phone').val().replace(/\D/g, "");
	send_to_number_valid = false;
	//ensure we are dealing with a US number with 10 digits
	if(/^[0-9]{10}$/.test(to_num)){
		send_to_number= '+1'+to_num;
		send_to_number_valid = true;
	}
	check_form();
}

//We only want to send a message if it actually has content
function validate_msg(){
	message_valid = false;
	if(!!$("#sms_msg").val()){
		message_valid = true;
	}
	check_form();
}


$(function() {

	//Hook up our event listeners
	$( "#send" ).click(function() {
		send_sms();
	});

	$( "#target_phone" ).change(function() {
		validate_phone();
	});
	$( "#sms_msg" ).keypress(function() {
		validate_msg();
	});
});