<?php

require __DIR__ . '/Twilio/autoload.php';
use Twilio\Rest\Client;

	class Req_Ob{
		public $sid;
		public $token;
		public $client;
		public $sender_num;
		public $result;

		public function __construct($id, $tk, $num) {
			$this->sid = $id;
			$this->token = $tk;
			$this->sender_num = $num;
			$this->client = new Twilio\Rest\Client($id, $tk);
			$this->result = (object) array(
								'action'=>'',
								'status'=>'Pending'
							);
        }

        public function send_sms($to,$msg){
        	$this->client->messages->create(
			    $to,
			    array(
			        'from' => $this->sender_num,
			        'body' => $msg
			    )
			);
			$this->result->action = 'send';
			$this->result->status = 'Success';
        }
        
	}

?>